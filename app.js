const express = require('express');
const axios   = require('axios');
const app     = express();
const API_URL = 'http://www.omdbapi.com/?apikey=dca61bcc'



app.get('/cari', (req, res) => {
    const { judul } = req.query;
    axios.get(`${API_URL}&s=${judul}`)
    .then(response => {
        if(response.data.Response == 'True') {
            const data_film = [];
            response.data.Search.map(data => {
                const { Title, Year, imdbID, Type, Poster } = data;
                data_film.push({
                    id: imdbID,
                    judul: Title,
                    tahun: Year,
                    type: Type,
                    banner: Poster
                })
            })
        
            res.status(200).json({
                status: true,
                status_code: 200,
                message: 'Judul film ditemukan!',
                data_film
            })
        }else{
            res.status(400).json({
                status: false,
                status_code: 400,
                message: 'Judul film tidak ditemukan!'
            })
        }
    })
    .catch(err => {
        console.log(err.message);
        res.status(500).json({
            status: false,
            status_code: 500,
            message: 'Internal Server Error'
        })
    })
})

app.get('/detail', (req, res) => {
    const { id } = req.query;
    axios.get(`${API_URL}&i=${id}`)
    .then(response => {
        const { 
            Response, 
            Title, 
            Year, 
            Rated, 
            Released, 
            Runtime, 
            Genre, 
            Director, 
            Writer, 
            Actors, 
            Plot, 
            Language, 
            Country, 
            Awards, 
            Poster, 
            Metascore, 
            imdbRating, 
            imdbVotes, 
            Type, 
            DVD, 
            BoxOffice, 
            Production 
        } = response.data
        if(Response == "True") {
            res.status(200).json({
                status: true,
                status_code: 200,
                message: 'Detail film ditemukan!',
                detail_film: {
                    judul: Title,
                    tahun: Year,
                    dinilai: Rated,
                    tahun_rilis: Released,
                    rentang_waktu: Runtime,
                    aliran: Genre,
                    direktur: Director,
                    penulis: Writer,
                    aktor: Actors,
                    deskripsi: Plot,
                    bahasa: Language,
                    negara: Country,
                    penghargaan: Awards,
                    banner: Poster,
                    skor_meta: Metascore,
                    rating: imdbRating,
                    suara: imdbVotes,
                    type: Type,
                    dvd_rilis: DVD,
                    harga: BoxOffice,
                    produksi: Production
                }
            })
        }else{
            res.status(400).json({
                status: false,
                status_code: 400,
                message: 'Detail film tidak ditemukan!'
            })
        }
    })
    .catch(err => {
        console.log(err.message);
        res.status(500).json({
            status: false,
            status_code: 500,
            message: 'Internal Server Error'
        })
    })
})

app.listen(8080, () => console.log('Running app on 8080'));